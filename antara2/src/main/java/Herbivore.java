public abstract class Herbivore extends Animal {
    @Override
    protected boolean IsGoodFood(Food food) {
        return food.getClass().getName().equals(HerbivoreFood.class.getName());
    }

    public int danger;
}
