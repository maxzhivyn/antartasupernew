import com.sun.javaws.IconUtil;

public class Main {
    public static void main(String[] args) throws Exception {
        //  Создал 2 вальера
        HerbivoreAviary herbivoreAviary = new HerbivoreAviary(3);
        PredatorAviary predatorAviary = new PredatorAviary(5);

        //  Наполнение животными
        herbivoreAviary.AddAnimal(new Cow());
        herbivoreAviary.AddAnimal(new Deer());
        herbivoreAviary.AddAnimal(new Elephant());
        herbivoreAviary.AddAnimal(new Cow()); // лишнее животное

        predatorAviary.AddAnimal(new Tiger());
        predatorAviary.isGoodAnimal(new Leopard());

        // корм
        herbivoreAviary.GetAnimals()[0].Eat(new HerbivoreFood());
        herbivoreAviary.GetAnimals()[0].Eat(new PredatorFood()); // не так еда
    }
}
