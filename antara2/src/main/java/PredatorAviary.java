public class PredatorAviary extends Aviary {

    public PredatorAviary(int size) {
        super(size);
    }

    @Override
    protected boolean isGoodAnimal(Animal animal) {
        return Predator.class.isAssignableFrom(animal.getClass());
    }
}
